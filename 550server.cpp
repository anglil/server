#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<string.h>
#include<sys/poll.h>
#include<unistd.h>
#include<fcntl.h>

#include<pthread.h>
#include<signal.h>

#include<list>
#include<queue>

#include "550server.h"
#include "taskQueue.h"
#include "resultList.h"
#include "connection.h"

using namespace std;

TaskQueue taskQueue;
ResultList resultList;

pthread_cond_t cv;
pthread_mutex_t dummyMutex;

const int BUFFERMAX = 65535;

/*
 * worker thread doing disk IO
 * */
void *workerThread(void*) {
	while(1) {
		// get a task from the taskQueue
		Task* myTask = taskQueue.getTask();
		if (myTask == NULL) {
			pthread_mutex_lock(&dummyMutex);
			pthread_cond_wait(&cv,&dummyMutex);
			pthread_mutex_unlock(&dummyMutex);
			continue;
		}
		cout << "get task" << endl;
		// read the file into memory
		FILE* file;
		char buf[BUFFERMAX];
		file = fopen(myTask->getFileName(), "r");
		int pos = myTask->getPos();
		int nextPos = -1;
		char* fileName = NULL;
		char* fileContent = NULL;
		int fileLength = 0;
		if (file != NULL) {
			fseek(file,pos,SEEK_SET);
			int byteCount = fread(buf,sizeof(char),BUFFERMAX,file);
			fileContent = new char[byteCount];			
			memcpy(fileContent,buf,byteCount);
			fileName = new char[strlen(myTask->getFileName())+1];
			memcpy(fileName,myTask->getFileName(),strlen(myTask->getFileName())+1);
			fileLength = byteCount;
			if (byteCount < BUFFERMAX) {
				nextPos = -1;
			} else {
				nextPos = pos + byteCount;
			}
			fclose(file);
		}
		// append the result to the resultList
		Result *myResult = new Result(myTask->getFd(), fileName, fileContent, fileLength, nextPos);
		resultList.addResult(myResult);		
		
		// send notification to the main thread
		int pipe = myTask->getPipe();
		int fd = myTask->getFd();		
		write(pipe, &fd, sizeof(int));
		close(pipe);
		cout << "notification sent" << endl;
		//clean up
		delete myTask;
	}
	pthread_exit(NULL);
}

int main(int argc, char* argv[])
{
	if (argc != 3) {
		cerr << "usage: ./550server ipaddress port" << endl;
		exit(-1);
	}
	// control the number of threads
	int numThreads = 4;

	int serverPort = atoi(argv[2]);
	struct sockaddr_in serverAddr;

	// set up the listening socket

	int listenfd = socket(AF_INET,SOCK_STREAM,0);
	fcntl(listenfd,F_SETFL,O_NONBLOCK);
	fcntl(listenfd,F_SETFL,O_ASYNC);
	bzero(&serverAddr,sizeof(serverAddr));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_addr.s_addr=htonl(INADDR_ANY);
	serverAddr.sin_port=htons(serverPort);
	if (bind(listenfd,(struct sockaddr*)&serverAddr, sizeof(serverAddr))<0) {
		cerr << "bind";
		exit(-1);
	}
	if (listen(listenfd,1024) < 0) {
		cerr << "listen";
		exit(-1);
	}
	
	// creat all the worker threads
	pthread_mutex_init(&dummyMutex, NULL);
	pthread_cond_init(&cv, NULL);
	pthread_t threads[numThreads];
	for (int i=0;i<numThreads;i++) {
		pthread_create(&threads[i], NULL, workerThread, NULL);
	}

	list<int> selfpipes;
	list<Connection> connections;
	
	struct sigaction act;
	memset(&act,0,sizeof(act));
	act.sa_handler = SIG_IGN;
	act.sa_flags = SA_RESTART;
	sigaction(SIGPIPE, &act, NULL);	
	while (1) {
		// set up the poll fds
		struct pollfd pollfds[1 + (int)connections.size() + (int)selfpipes.size()];
		pollfds[0].fd = listenfd;
		pollfds[0].events = POLLIN;
		pollfds[0].revents = 0;
		int index = 1;
		for (list<Connection>::iterator it = connections.begin() ; it != connections.end(); it++) {
			pollfds[index].fd = it->getFd();
			if (it->getSend() == true) {
				pollfds[index].events = POLLIN | POLLOUT;
			} else {
				pollfds[index].events = POLLIN;
			}
			pollfds[index].revents = 0;
			index ++;
		}
		for (list<int>::iterator it = selfpipes.begin() ; it != selfpipes.end(); it++) {
			pollfds[index].fd = *it;
			pollfds[index].events = POLLIN;
			pollfds[index].revents = 0;
			index ++;
		}

		if (1+(int)connections.size()+(int)selfpipes.size() != index) {
			cout << "failure" << endl;
		}

		// wait for any kind of asynchronous IO
		int flag = poll(pollfds, 1 + connections.size() + selfpipes.size(), -1);
		if (flag < 0) {
			cerr << "poll";
			exit(-1);
		} else if (flag == 0) {
			cout << "Nothing needs to be done!!" << endl;
		} else {
			int flag2 = 0;
			if (pollfds[0].revents & POLLIN) {
				// accept client connections
				struct sockaddr_in clientAddr;
				socklen_t clientAddrSize = sizeof(clientAddr);
				int fd = accept(listenfd, (struct sockaddr*)&clientAddr,&clientAddrSize);
				Connection connection(fd);
				connections.push_front(connection);
				continue;
			}
			for (int i=1+(int)connections.size();i<1+(int)connections.size()+(int)selfpipes.size();i++) {
				if (pollfds[i].revents & POLLIN) {
					// set up a flag in the connection list so that we will wait for a good time to write to the socket
					cout << "disk completes" << endl;
					int fd = 0;
					read(pollfds[i].fd, &fd, sizeof(int));
					close(pollfds[i].fd);
					for (list<Connection>::iterator it = connections.begin() ; it != connections.end(); it++) {
						if (it->getFd() == fd) {
							it->send();
						}
					}
					selfpipes.remove(pollfds[i].fd);		
					flag2 = 1;
					break;
				}
			}
			if (flag2 == 1) {
				continue;
			}
			for (int i=1;i<1+(int)connections.size();i++) {
				if (pollfds[i].revents & POLLIN) {
					// read from the client
					char buf[BUFFERMAX];
					int byteCount = 0;
					byteCount = recv(pollfds[i].fd, buf, sizeof(buf),0);
					char* filename = new char[byteCount-1];
					memcpy(filename,buf,byteCount-1);
					filename[byteCount-2] = '\0';
					int pipefd[2];
					if (pipe(pipefd) == -1) {
						cerr << "pipe" << endl;
						exit(-1);
					}
					selfpipes.push_front(pipefd[0]);
					Task *myTask = new Task(pollfds[i].fd,filename,pipefd[1],0);
					taskQueue.addTask(myTask);
					pthread_cond_broadcast(&cv);

					flag2 = 1;
					break;
				}
				if (pollfds[i].revents & POLLOUT) {
					// write to the client
					Result *myResult = resultList.getResult(pollfds[i].fd);
					if (myResult == NULL) {
						continue;
					}
					if (myResult->getFileContent() == NULL) {
						delete myResult;
						close(pollfds[i].fd);
						for (list<Connection>::iterator it = connections.begin() ; it != connections.end(); it++) {
							if (it->getFd() == pollfds[i].fd) {
								connections.erase(it);
								break;
							}
						}
						flag2 = 1;
						break;
					}				
					send(myResult->getFd(), myResult->getFileContent(), myResult->getFileLength(), 0);
					for (list<Connection>::iterator it = connections.begin() ; it != connections.end(); it++) {
						if (it->getFd() == pollfds[i].fd) {
							it->sendCompletes();
						}
					}
					if(myResult->getPos() > 0) {
						int pipefd[2];
						if (pipe(pipefd) == -1) {
							cerr << "pipe" << endl;
							exit(-1);
						}
						selfpipes.push_front(pipefd[0]);
						char* fileName = new char[strlen(myResult->getFileName()) + 1];
						memcpy(fileName,myResult->getFileName(),strlen(myResult->getFileName()) + 1);
						Task *myTask = new Task(pollfds[i].fd,fileName,pipefd[1],myResult->getPos());
						taskQueue.addTask(myTask);
						pthread_cond_broadcast(&cv);
						delete myResult;
						flag2 = 1;
						break;	
					} else {
						cout << "file sending completes" << endl;
						close(pollfds[i].fd);
						for (list<Connection>::iterator it = connections.begin() ; it != connections.end(); it++) {
							if (it->getFd() == pollfds[i].fd) {
								connections.erase(it);
								break;
							}
						}
						delete myResult;
						flag2 = 1;
						break;
					}
				}
			}
			if (flag2 == 1) {
				continue;
			}

		}
	}
	// clean up
	pthread_cond_destroy(&cv);
	pthread_mutex_destroy(&dummyMutex);
	return 0;
}



