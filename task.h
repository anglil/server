class Task{
	private:
	int fd;
	char* fileName;
	int pos;
	int pipe;
	public:
	Task(int, char*,int,int);
	~Task();
	int getFd();
	char* getFileName();
	int getPos();
	int getPipe();
	
};
