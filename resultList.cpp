#include "resultList.h"
#include <string.h>
#include <stdlib.h>
ResultList::ResultList() {
	pthread_mutex_init(&resultLock,NULL);
}
ResultList::~ResultList() {
	pthread_mutex_destroy(&resultLock);
}
Result* ResultList::getResult(int fd) {
	pthread_mutex_lock(&resultLock);
	if (resultList.size() == 0) {
		pthread_mutex_unlock(&resultLock);
		return NULL;
	}
	for (list<Result*>::iterator it = resultList.begin(); it != resultList.end(); it++) {
		if ((*it)->getFd() == fd) {
			Result *returnResult = *it;
			resultList.erase(it);
			pthread_mutex_unlock(&resultLock);
			return returnResult;

		}
	}
	pthread_mutex_unlock(&resultLock);
	return NULL;
}
void ResultList::addResult(Result* result){
	pthread_mutex_lock(&resultLock);
	resultList.push_back(result);
	pthread_mutex_unlock(&resultLock);
}



