CC = g++
CFLAGS = -c -g -Wall
all: 550server

550server: 550server.o task.o taskQueue.o result.o resultList.o connection.o
	$(CC) 550server.o task.o taskQueue.o result.o resultList.o connection.o -lpthread -o 550server

550server.o : 550server.cpp
	$(CC) $(CFLAGS) 550server.cpp

task.o : task.h task.cpp
	$(CC) $(CFLAGS) task.cpp

taskQueue.o : taskQueue.h taskQueue.cpp
	$(CC) $(CFLAGS) taskQueue.cpp

result.o : result.h result.cpp
	$(CC) $(CFLAGS) result.cpp

resultList.o : resultList.h resultList.cpp
	$(CC) $(CFLAGS) resultList.cpp

connection.o : connection.h connection.cpp
	$(CC) $(CFLAGS) connection.cpp

clean:
	rm -rf *o 550server
