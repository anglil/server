class Result{
	private:
	int fd;
	char* fileName;
	char* fileContent;
	int fileLength;
	int pos;
	public:
	Result(int, char*, char*,int,int);
	~Result();
	int getFd();
	char* getFileName();
	char* getFileContent();
	int getFileLength();
	int getPos();
};
