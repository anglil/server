#include "result.h"
#include <stdlib.h>

Result::Result(int f, char* fn, char* fc, int fl, int po) {
	fd = f;
	fileName = fn;
	fileContent = fc;
	fileLength = fl;
	pos = po;
}
Result::~Result() {
	delete[] fileName;
	delete[] fileContent;
}
int Result::getFd() {
	return fd;
}
char* Result::getFileName() {
	return fileName;
}
char* Result::getFileContent() {
	return fileContent;
}
int Result::getFileLength() {
	return fileLength;
}
int Result::getPos() {
	return pos;
}
