#include "task.h"
#include <pthread.h>
#include <queue>

using namespace std;

/*
 * atomic implementation of a task queue
 */

class TaskQueue{
	private:
	pthread_mutex_t taskLock;
	queue<Task*> taskQueue;
	public:
	TaskQueue();
	~TaskQueue();
	Task *getTask();
	void addTask(Task*);	
};
