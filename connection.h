class Connection{
	private:
	int fd;
	bool sendPending;
	public:
	Connection(int);
	~Connection();
	int getFd();
	void send();
	void sendCompletes();
	bool getSend();
};
