#include "connection.h"
#include <stdlib.h>

Connection::Connection(int f) {
	fd = f;
	sendPending = false;
}
Connection::~Connection() {
}
int Connection::getFd() {
	return fd;
}
void Connection::send(){
	sendPending = true;
}
void Connection::sendCompletes(){
	sendPending = false;
}
bool Connection::getSend(){
	return sendPending;
}

