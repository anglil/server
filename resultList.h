#include "result.h"
#include <pthread.h>
#include <list>

using namespace std;

/*
 * Implements a atomic list that will store the result of disk IO
 */

class ResultList{
	private:
	pthread_mutex_t resultLock;
	list<Result*> resultList;
	public:
	ResultList();
	~ResultList();
	Result* getResult(int);
	void addResult(Result*);	
};
