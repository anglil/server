#include "taskQueue.h"
#include <string.h>
#include <stdlib.h>
TaskQueue::TaskQueue() {
	pthread_mutex_init(&taskLock,NULL);
}
TaskQueue::~TaskQueue() {
	pthread_mutex_destroy(&taskLock);
}
Task* TaskQueue::getTask() {
	pthread_mutex_lock(&taskLock);
	if (taskQueue.size() == 0) {
		pthread_mutex_unlock(&taskLock);
		return NULL;
	}
	Task *returnTask = taskQueue.front();
	taskQueue.pop();
	pthread_mutex_unlock(&taskLock);
	return returnTask;
}
void TaskQueue::addTask(Task* task){
	pthread_mutex_lock(&taskLock);
	taskQueue.push(task);
	pthread_mutex_unlock(&taskLock);
}



