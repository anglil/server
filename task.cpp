#include "task.h"
#include <stdlib.h>

Task::Task(int f, char* n,int p, int po) {
	fd = f;
	fileName = n;
	pipe = p;
	pos = po;
}
Task::~Task() {
	delete[] fileName;
}
int Task::getFd() {
	return fd;
}
char* Task::getFileName(){
	return fileName;
}
int Task::getPipe() {
	return pipe;
}
int Task::getPos() {
	return pos;
}
